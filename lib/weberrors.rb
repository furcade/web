require 'uri'
module WebErrors
  INVALID_PASSWORD = URI.escape("Passwords must be at least 6 characters long.")
  INVALID_USERNAME = URI.escape("Usernames can only contain lowercase letters, numbers, and dashes, cannot start or end with a dash, and must be at least 2 characters long.")
  PASSWORDS_DONT_MATCH = URI.escape("Passwords do not match.")
  USERNAME_TAKEN = URI.escape("A user with that name already exists.")
  BAD_CREDENTIALS = URI.escape("Your username or password is incorrect.")
  BAD_LOCKWORD = URI.escape("Incorrect LOCKWORD.")
  BAD_PASSWORD = URI.escape("Incorrect password.")
end

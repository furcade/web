require 'digest'
require 'securerandom'
require './lib/weberrors'
def single_result data
  data.to_a[0]
end
class Session
  def initialize client, id
    @client = client
    @id = id
    @username = single_result(client[:sessions].find(:_id => id))
  end
  def id
    @id
  end
  def self.session_exists? client, sessionid
    client[:sessions].find(:_id => sessionid).count > 0
  end
  def self.spawn client, username
    result = client[:sessions].insert_one({
      :username => username,
      :touched => Time.now.to_i
    })
    return Session.new client, result.inserted_id
  end
  def self.touch client, sessionid
    client[:sessions].find(:_id => sessionid).update_one({ "$set" => { :touched => Time.now.to_i } })
  end
  def self.delete client, sessionid
    client[:sessions].find(:_id => sessionid).delete_one
  end
  # Start a slow loop to delete sessions after they haven't been 'touched' in ~ 1 month.
  def self.expiry_thread client
    Thread.new do
      client[:sessions].find({ :touched => { "$lt" => Time.now.to_i - 259200 } }).delete_many
      sleep(100000)
    end
  end
end
class User
  def initialize client, username
    @client = client
    @username = username
  end

  def username
    @username
  end

  def data
    x = single_result(@client[:users].find(:username => @username))
    puts @username
    puts x.inspect
    x
  end

  def password_is? password
    Digest::SHA256.hexdigest(password + data[:salt]) == data[:passhash]
  end

  def set_password password
    if User.valid_password? password
      pass_hex = Digest::SHA256.hexdigest(password + data[:salt])
      @client[:users].find(:username => @username).update_one({ "$set" => { :passhash => pass_hex } })
    else
      return WebErrors::INVALID_PASSWORD
    end
  end

  def delete_account
    @client[:users].find(:username => @username).delete_one
  end

  def self.valid_password? password
    password.strip != '' && password.length > 5
  end

  def self.valid_username? username
    /^[a-z0-9][a-z0-9-]*[a-z0-9]$/ =~ username
  end

  def self.create client, username, password, pass2
    return WebErrors::USERNAME_TAKEN if client[:users].find(:username => username).count > 0
    return WebErrors::PASSWORDS_DONT_MATCH if password != pass2
    return WebErrors::INVALID_PASSWORD if !valid_password? password
    if !valid_username? username
      return WebErrors::INVALID_USERNAME
    end
    salt = SecureRandom.base64 64
    client[:users].insert_one({
      :username => username,
      :passhash => Digest::SHA256.hexdigest(password + salt),
      :salt => salt,
      :sessions => []
    })
    User.new client, username
    User.login client, username, password
  end

  def self.by_sessionid client, sessionid
    return nil if sessionid == nil

    if Session.session_exists? client, sessionid
      result = client[:sessions].find({ :_id => sessionid })
      Session.touch client, sessionid
      puts " -- USER:::: " + single_result(result).inspect
      User.new(client, single_result(result)[:username])
    else
      nil
    end
  end

  def self.login client, username, password
    error_invalid = WebErrors::BAD_CREDENTIALS
    data = client[:users].find(:username => username)
    if data.count > 0
      user = User.new client, username
      if user.password_is? password
        ss = Session.spawn(client, username).id
        puts ss
        return ss
      else
        return error_invalid
      end
    else
      return error_invalid
    end
  end
end

require 'uri'
def css url
  "<link rel='stylesheet' href='#{url}'>"
end
def nav authenticated
  links = [
    ['home', '/'],
    ['log in', '/login', false],
    ['register', '/register', false],
    ['settings', '/settings', true],
    ['log out', '/logout', true]
  ]
  elements = []
  links.each do |link|
    if link[2] == nil || (link[2] == true && authenticated) || (link[2] == false && !authenticated)
      elements.push "<a href='#{link[1]}'>#{link[0]}</a>"
    end
  end
  "<div class='header'><div class='title'>furcade</div><div class='nav'>" + elements.join(" <span class='navslash'>/</span> ") + "</div></div>"
end
def errorbox error
  if error != nil
    "<div class='errorbox'>#{error}</div>"
  else
    ""
  end
end

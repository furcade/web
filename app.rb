require 'sinatra/base'
require 'mongo'
require 'rack/session/moneta'
require './lib/user'
require './lib/pagehelpers'
require './lib/weberrors'
class App < Sinatra::Base
  set :sessions => true

  Mongo::Logger.logger.level = ::Logger::FATAL
  uri = ENV["MONGODB_URI"] != nil ? ENV["MONGODB_URI"] : "mongodb://127.0.0.1:27017/mydb"
  client = Mongo::Client.new(uri)

  # temporarily disabled (not working)
  # use Rack::Session::Moneta, store: Moneta.new(:Mongo, backend: client)

  # A password for use on locked pages.
  lockword = ENV["LOCKWORD"] != nil ? ENV["LOCKWORD"] : "locked"

  # Start the old session killer.
  Session.expiry_thread client

  register do
    def auth (type)
      condition do
        redirect "/login" unless send("is_#{type}?")
      end
    end
  end

  helpers do
    def is_user?
      @user != nil
    end
    def follow_then_to
      if params["then_to"] == nil || params["then_to"] == ""
        redirect to "/"
      else
        redirect to params["then_to"]
      end
    end
  end

  before do
    @user = User.by_sessionid(client, session[:user_id])
  end

  get "/" do
    haml :home
  end

  get "/settings", :auth => :user do
    haml :settings
  end

  post '/settings/password', :auth => :user do
    if @user.password_is? params["oldpass"]
      data = @user.set_password params["newpass"]
      if data.is_a? String
        redirect to ("/settings?passworderror=" + data)
      else
        redirect to "/settings"
      end
    else
      redirect to ("/settings?passworderror=" + WebErrors::BAD_PASSWORD + "#password")
    end
  end

  post '/settings/delete', :auth => :user do
    if @user.password_is? params["password"]
      @user.delete_account
      Session.delete client, session[:user_id]
      redirect to ("/")
    else
      redirect to ("/settings?deleteerror=" + WebErrors::BAD_PASSWORD + "#delete")
    end
  end

  get "/login" do
    haml :login
  end

  post "/login/auth" do
    data = User.login(client, params["username"], params["password"]) # User.authenticate(params).id
    if data.is_a? String
      redirect to ("/login?then_to=" + params["then_to"] + "&error=" + data)
    else
      session[:user_id] = data
      follow_then_to
    end
  end

  get "/register" do
    haml :register
  end

  post "/register/auth" do
    if params["lockword"] == lockword
      data = User.create(client, params["username"], params["password"], params["pass2"]) # User.authenticate(params).id
      if data.is_a? String
        redirect to ("/register?then_to=" + params["then_to"] + "&error=" + data)
      else
        session[:user_id] = data
        puts "FOO"
        follow_then_to
      end
    else
      redirect to("/register?then_to=" + params["then_to"] + "&error=" + WebErrors::BAD_LOCKWORD)
    end
  end

  get "/logout" do
    Session.delete client, session[:user_id]
    session[:user_id] = nil
    follow_then_to
  end
  run!
end
